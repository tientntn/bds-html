$(document).ready(function () {
  // Menu
  $(window).on("scroll", function () {
    if ($(window).scrollTop() >= 20) {
      $(".navbar-wrap").addClass("nav-fix");
    } else {
      $(".navbar-wrap").removeClass("nav-fix");
    }
  });
});
