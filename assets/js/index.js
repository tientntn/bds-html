var map = null;
var areas = [
  {
    lat: 16.0473224,
    lng: 108.1366648,
    image: "./assets/images/thumb_infowindow.png",
    name: "KCN Nam Đàn",
    price: "$999/m2",
  },
  {
    lat: 16.9,
    lng: 107,
    image: "./assets/images/thumb_infowindow.png",
    name: "Khu công nghiệp deep C Quảng Ninh",
    price: "$999/m2",
  },
  {
    lat: 12,
    lng: 107,
    image: "./assets/images/thumb_infowindow.png",
    name: "Khu công nghiệp HCM",
    price: "$999/m2",
  },
];
var infowindow = {};
var markers = [];
function initMap() {
  var MapOptions = {
    zoom: 6,
    center: new google.maps.LatLng(16.0473224, 108.1366648),
    scrollwheel: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    backgroundColor: "#FFF",
    fullscreenControl: false,
    scaleControl: true,
    zoomControl: false,
    streetViewControl: false,
    disableDefaultUI: true,
  };
  var styleArray = [
    {
      featureType: "poi.business",
      stylers: [
        {
          visibility: "off",
        },
      ],
    },
    {
      featureType: "poi.park",
      elementType: "labels.text",
      stylers: [
        {
          visibility: "off",
        },
      ],
    },
  ];
  map = new google.maps.Map(document.getElementById("map"), MapOptions);
  map.setOptions({
    styles: styleArray,
  });
  infowindow = new google.maps.InfoWindow({
    content: "",
  });
  google.maps.event.addListener(map, "click", function (event) {
    infowindow.close();
  });
}

var projectSwiper = new Swiper("#home-project-slide", {
  slidesPerView: "auto",
  spaceBetween: 16,
  breakpoints: {
    768: {
      slidesPerView: 2,
      spaceBetween: 20,
      grid: {
        rows: 2,
      },
    },
    1024: {
      slidesPerView: 3,
      spaceBetween: 32,
      grid: {
        rows: 2,
      },
    },
  },
});
var projectTypeSwiper = new Swiper("#home-project-type-slide", {
  slidesPerView: 2,
  spaceBetween: 10,
  breakpoints: {
    768: {
      slidesPerView: "auto",
      spaceBetween: 16,
    },
    1024: {
      slidesPerView: "auto",
      spaceBetween: 16,
    },
  },
});

var projectFirstSwiper = new Swiper("#home-project-first-slide", {
  slidesPerView: "auto",
  spaceBetween: 16,
  breakpoints: {
    768: {
      slidesPerView: 2,
      spaceBetween: 20,
      grid: {
        rows: 2,
      },
    },
    1024: {
      slidesPerView: 3,
      spaceBetween: 32,
      grid: {
        rows: 2,
      },
    },
  },
});
var projectFirstTypeSwiper = new Swiper("#home-project-first-type-slide", {
  slidesPerView: 2,
  spaceBetween: 10,
  grid: {
    rows: 2,
  },
  breakpoints: {
    768: {
      slidesPerView: "auto",
      spaceBetween: 16,
      grid: {
        rows: 1,
      },
    },
    1024: {
      slidesPerView: "auto",
      spaceBetween: 16,
      grid: {
        rows: 1,
      },
    },
  },
});

var factorySwiper = new Swiper("#home-factory-slide", {
  slidesPerView: 1,
  spaceBetween: 16,
  grid: {
    rows: 2,
  },
  breakpoints: {
    768: {
      slidesPerView: 1,
      spaceBetween: 16,
      grid: {
        rows: 2,
      },
    },
    1024: {
      slidesPerView: 2,
      spaceBetween: 32,
      grid: {
        rows: 2,
      },
    },
  },
});
var factoryTypeSwiper = new Swiper("#home-factory-type-slide", {
  slidesPerView: "auto",
  spaceBetween: 10,
  breakpoints: {
    768: {
      slidesPerView: "auto",
      spaceBetween: 16,
      grid: {
        rows: 1,
      },
    },
    1024: {
      slidesPerView: "auto",
      spaceBetween: 16,
      grid: {
        rows: 1,
      },
    },
  },
});
var newsCategorySwiper = new Swiper("#home-news-categories-slide", {
  slidesPerView: 2,
  spaceBetween: 10,
  grid: {
    rows: 2,
  },
  breakpoints: {
    768: {
      slidesPerView: "auto",
      spaceBetween: 16,
      grid: {
        rows: 1,
      },
    },
    1024: {
      slidesPerView: "auto",
      spaceBetween: 16,
      grid: {
        rows: 1,
      },
    },
  },
});

var homeNewsSwiper = new Swiper("#home-news-slide", {
  autoplay: {
    delay: 3000,
    disableOnInteraction: false,
  },
  loop: true,
  slidesPerView: "auto",
  spaceBetween: 16,
  // effect: "coverflow",
  // coverflowEffect: {
  //   scale: 0.85,
  //   rotate: 0,
  //   stretch: 0,
  //   depth: 100,
  //   modifier: 1,
  //   slideShadows: false,
  // },
  breakpoints: {
    768: {
      slidesPerView: 3,
      spaceBetween: 20,
      // effect: "coverflow",
      // coverflowEffect: {
      //   scale: 1,
      //   rotate: 0,
      //   stretch: 0,
      //   depth: 0,
      //   modifier: 1,
      //   slideShadows: false,
      // },
    },
    1024: {
      slidesPerView: 4,
      spaceBetween: 32,
      // effect: "coverflow",
      // coverflowEffect: {
      //   scale: 1,
      //   rotate: 0,
      //   stretch: 0,
      //   depth: 0,
      //   modifier: 1,
      //   slideShadows: false,
      // },
    },
  },
  pagination: {
    el: "#home-new-pagination",
    type: "bullets",
    clickable: true,
  },
});

var partnerCategorySwiper = new Swiper("#home-partner-categories-slide", {
  slidesPerView: 2,
  spaceBetween: 10,
  grid: {
    rows: 2,
  },
  breakpoints: {
    768: {
      slidesPerView: "auto",
      spaceBetween: 16,
      grid: {
        rows: 1,
      },
    },
    1024: {
      slidesPerView: "auto",
      spaceBetween: 16,
      grid: {
        rows: 1,
      },
    },
  },
});
var partnersSwiper = new Swiper("#partners-slide", {
  slidesPerView: 2,
  spaceBetween: 16,
  autoplay: {
    delay: 4000,
    disableOnInteraction: false,
  },
  loop: true,
  loop: true,
  breakpoints: {
    768: {
      slidesPerView: 4,
      spaceBetween: 20,
    },
    1024: {
      slidesPerView: 6,
      spaceBetween: 28,
    },
  },
  pagination: {
    el: "#partners-slide-pagination",
    type: "bullets",
    clickable: true,
  },
});
$(document).ready(function () {
  appInit();
  $("#map-control-fullscreen").click(function () {
    document.getElementById("map").requestFullscreen();
  });
  $("#zoom-in").click(function () {
    if (map) {
      map.setZoom(map.getZoom() + 1);
    }
  });
  $("#zoom-out").click(function () {
    if (map) {
      map.setZoom(map.getZoom() - 1);
    }
  });
  $(".box-map-area").hover(function () {
    let index = $(this).attr("data-index");
    if (markers[index]) {
      map.panTo(markers[index].getPosition());
      infowindow.setContent(markers[index].content);
      infowindow.open(map, markers[index]);
    }
  });

  // sticky
  // var Sticky = new hcSticky("#sidebar-right", {
  //   // top: 160,
  //   bottom: 0,
  //   // followScroll: false,
  //   // stickTo: "news-detail-page",
  //   // responsive: {
  //   //   980: {
  //   //     disable: true,
  //   //   },
  //   // },
  // });

  $(".sidebar-right").theiaStickySidebar({
    additionalMarginTop: 150,
  });

  $(".date-picker-nk").datepicker({
    format: "dd/mm/yyyy",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true,
    weekStart: 1,
    language: "vi",
    // language: "en",
    // language: "ja",
    // language: "ko",
    // language: "zh-CN",
  });
  $("#home-project-first-slide-btn-left").click(function () {
    $("#home-project-first-slide-btn-right").removeClass("active");
    $("#home-project-first-slide-btn-left").removeClass("active");
    $(this).addClass("active");
    projectFirstSwiper.slidePrev();
  });

  $("#home-project-first-slide-btn-right").click(function () {
    $("#home-project-first-slide-btn-right").removeClass("active");
    $("#home-project-first-slide-btn-left").removeClass("active");
    $(this).addClass("active");
    projectFirstSwiper.slideNext();
  });

  $("#home-project-slide-btn-left").click(function () {
    $("#home-project-slide-btn-right").removeClass("active");
    $("#home-project-slide-btn-left").removeClass("active");
    $(this).addClass("active");
    projectSwiper.slidePrev();
  });

  $("#home-project-slide-btn-right").click(function () {
    $("#home-project-slide-btn-right").removeClass("active");
    $("#home-project-slide-btn-left").removeClass("active");
    $(this).addClass("active");
    projectSwiper.slideNext();
  });

  $(".add-on").click(function () {
    $(".add-on-list").toggleClass("active");
  });

  $(".btn-home-factory-type").click(function () {
    $(".btn-home-factory-type").removeClass("active");
    $(this).addClass("active");
  });
  $(".btn-home-partner-categories").click(function () {
    $(".btn-home-partner-categories").removeClass("active");
    $(this).addClass("active");
  });
  $(".btn-home-news-categories").click(function () {
    $(".btn-home-news-categories").removeClass("active");
    $(this).addClass("active");
  });
  $(".btn-home-project-first-type").click(function () {
    $(".btn-home-project-first-type").removeClass("active");
    $(this).addClass("active");
  });
  $(".btn-home-project-type").click(function () {
    $(".btn-home-project-type").removeClass("active");
    $(this).addClass("active");
  });

  $(".btn-view-type-list").click(function () {
    $(".btn-view-type").removeClass("active");
    $(".bds-project-type-net").addClass("d-none");
    $(".bds-project-type-list").removeClass("d-none");
    $(this).addClass("active");
  });
  $(".btn-view-type-net").click(function () {
    $(".btn-view-type").removeClass("active");
    $(".bds-project-type-net").removeClass("d-none");
    $(".bds-project-type-list").addClass("d-none");
    $(this).addClass("active");
  });
  $("#luxury-slide-preview").click(function () {
    $("#luxury-slide-next").removeClass("luxury-active");
    $("#luxury-slide-preview").removeClass("luxury-active");
    $(this).addClass("luxury-active");
  });
  $("#luxury-slide-next").click(function () {
    $("#luxury-slide-next").removeClass("luxury-active");
    $("#luxury-slide-preview").removeClass("luxury-active");
    $(this).addClass("luxury-active");
  });

  $(document).on("click", ".bsnav-mobile.in .nav-link-parent", function (e) {
    e.preventDefault();
    let check = false;
    if (
      $(this)
        .parent()
        .children(".navbar-nav-nk-wrapper")
        .hasClass("show-navbar-nav-nk")
    ) {
      check = true;
    }
    $(".navbar-nav-nk-wrapper").removeClass("show-navbar-nav-nk");
    if (check) {
      $(this)
        .parent()
        .children(".navbar-nav-nk-wrapper")
        .removeClass("show-navbar-nav-nk");
    } else {
      $(this)
        .parent()
        .children(".navbar-nav-nk-wrapper")
        .addClass("show-navbar-nav-nk");
    }
  });

  var lastScrollTop = 0;
  $(window).scroll(function (event) {
    var st = $(this).scrollTop();
    if (st > lastScrollTop) {
      lastScrollTop = st - 15;
      $(".header-top").addClass("hide");
    } else {
      lastScrollTop = st + 15;
      $(".header-top").removeClass("hide");
    }
  });

  var hasRunCount = false;
  counter();
  $(window).scroll(function () {
    counter();
  });

  function counter() {
    if ($("#static-block-list").offset()) {
      var top_of_element = $("#static-block-list").offset().top;
      var bottom_of_element =
        $("#static-block-list").offset().top +
        $("#static-block-list").outerHeight();
      var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
      var top_of_screen = $(window).scrollTop();

      if (
        bottom_of_screen > top_of_element &&
        top_of_screen < bottom_of_element &&
        !hasRunCount
      ) {
        hasRunCount = true;
        const counterw = document.querySelectorAll(".count-up");

        counterw.forEach((counter) => {
          const updateCount = () => {
            const target = parseInt(counter.getAttribute("data-target"));
            const speed = parseFloat(counter.getAttribute("data-speed"));
            const run = parseInt(counter.getAttribute("data-run"));
            const count = parseInt(counter.innerText);
            const increment = Math.trunc(target / speed);

            if (count < target) {
              counter.innerText = count + increment;
              setTimeout(updateCount, run);
            } else {
              counter.innerText = target;
            }
          };
          updateCount();
        });
      }
    }
  }
});

function getMarkers() {
  for (var i = 0; i < areas.length; i++) {
    let infowindowContent =
      '<div class="infowindow d-flex">' +
      '<img src="' +
      areas[i].image +
      '" class="thumb"  alt="thumb" />' +
      '<div class="content">' +
      '<p class="name">' +
      areas[i].name +
      "</p>" +
      '<p class="price">' +
      areas[i].price +
      "</p>" +
      '<a href="/" class="view-detail">Xem chi tiết</a>' +
      " </div></div>";
    const marker = new google.maps.Marker({
      position: new google.maps.LatLng(areas[i].lat, areas[i].lng),
      icon: {
        url: "./assets/images/map_pin.png",
        // size: new google.maps.Size(9, 9),
        scaledSize: new google.maps.Size(25, 25),
        anchor: new google.maps.Point(0, 0),
      },
      map: map,
      lat: areas[i].lat,
      lng: areas[i].lng,
      content: infowindowContent,
    });
    marker.addListener("mouseover", function () {
      infowindow.setContent(infowindowContent);
      infowindow.open(map, marker);
    });
    marker.addListener("click", function () {
      infowindow.setContent(infowindowContent);
      infowindow.open(map, marker);
    });
    markers.push(marker);
  }
}

function appInit() {
  getMarkers();

  var mainSlide = new Swiper("#main-slide", {
    slidesPerView: "auto",
    slidesPerView: 1,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
    },
    loop: true,
    pagination: {
      el: "#main-slide-pagination",
      type: "bullets",
      clickable: true,
    },
  });
  var luxurySlide = new Swiper("#luxury-slide", {
    slidesPerView: 2,
    spaceBetween: 16,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false,
    },
    breakpoints: {
      768: {
        slidesPerView: 3,
        spaceBetween: 24,
      },
      1024: {
        slidesPerView: 4,
        spaceBetween: 32,
      },
    },
    loop: true,
    navigation: {
      nextEl: "#luxury-slide-next",
      prevEl: "#luxury-slide-preview",
    },
  });

  var detailThumb = new Swiper("#detail-thumb-slide", {
    loop: true,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
    spaceBetween: 16,
    breakpoints: {
      768: {
        spaceBetween: 16,
      },
      1024: {
        spaceBetween: 32,
      },
    },
  });
  var detailSlide = new Swiper("#detail-slide", {
    loop: true,
    // autoplay: {
    //   delay: 3000,
    //   disableOnInteraction: false,
    // },
    spaceBetween: 10,
    navigation: {
      nextEl: ".detail-swiper-button-next",
      prevEl: ".detail-swiper-button-prev",
    },
    thumbs: {
      swiper: detailThumb,
    },
  });

  var newsDetailThumb = new Swiper("#news-detail-thumb-slide", {
    loop: true,
    freeMode: true,
    watchSlidesProgress: true,
    spaceBetween: 16,
    slidesPerView: 4,
    breakpoints: {
      768: {
        spaceBetween: 16,
        slidesPerView: 4,
      },
      1024: {
        spaceBetween: 32,
        slidesPerView: 3,
      },
    },
  });

  var newsDetailSlide = new Swiper("#news-detail-slide", {
    loop: true,
    spaceBetween: 10,
    navigation: {
      nextEl: ".detail-swiper-button-next",
      prevEl: ".detail-swiper-button-prev",
    },
    thumbs: {
      swiper: newsDetailThumb,
    },
  });
}
